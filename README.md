![Nossa logo](/logo_funto_amarelo.png)

## Instalação

### Instalação do Node

No terminal digite:

```curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash```

Se não tiver o `curl` instalado tente por:

```wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash```

Feche e abra o terminal

```nvm install node```

## Instalação do Adonis

Execute os seguintes comandos para instalação do Adonis:

```sudo apt install postgresql postgresql-contrib```

```sudo -u postgres psql```

```ALTER USER postgres WITH PASSWORD 'postgres';```

```CREATE DATABASE beerbud```

```exit()```

```beerbud-api -> sessions```

```npm install```

```npm i -g @adonisjs/cli```

```adonis migration:status```

```adonis migration:run```

```Dbeaver```

```adonis make:model Products -m -c```

### Instalação das dependências de python

```pip3 install matplotlib```

```pip3 install -U Pillow```

```pip3 install numpy```

```pip3 install torch```

```pip3 install torchvision```

### Clonando o repositório e instalando suas dependencias

### Instalação das dependências``

Clone o repositório https://gitlab.com/hacky-hour/beerbud.git

Para instalar os pacotes necessários execute no diretório `beerbud`:

```npm install```


Run the following command to run startup migrations.

```js
adonis migration:run
```
