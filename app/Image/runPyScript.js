'use strict'

let {PythonShell} = require('python-shell') 

class Python {
  async run(path1, path2){
    return new Promise(resolve => {
      let pyshell = new PythonShell("app/Image/glueImages.py");
      pyshell.send(`app/Image/input/${path1}.jpg\napp/Image/input/${path2}.jpg\n`)

      pyshell.end(function (err,code,signal) {
        if (err) throw err;
        console.log('The exit code was: ' + code);
        console.log('The exit signal was: ' + signal);
        console.log('finished');
        resolve(true)
      });
    })
  }
}

module.exports = Python