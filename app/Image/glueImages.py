import PIL
from PIL import Image
import matplotlib.pyplot as plt
import torch
import torchvision.transforms as T
from torchvision import models
import numpy as np
import sys
import os

# Define the helper function
def decode_segmap(image, nc=21):
  
  label_colors = np.array([(0, 0, 0),  # 0=background
               # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
               (128, 0, 0), (0, 128, 0), (128, 128, 0), (0, 0, 128), (192, 128, 128),
               # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
               (0, 128, 128), (128, 128, 128), (64, 0, 0), (192, 0, 0), (64, 128, 0),
               # 11=dining table, 12=dog, 13=horse, 14=motorbike, 15=person
               (192, 128, 0), (64, 0, 128), (192, 0, 128), (64, 128, 128), (192, 128, 128),
               # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor
               (0, 64, 0), (128, 64, 0), (0, 192, 0), (128, 192, 0), (0, 64, 128)])

  r = np.zeros_like(image).astype(np.uint8)
  g = np.zeros_like(image).astype(np.uint8)
  b = np.zeros_like(image).astype(np.uint8)
  
  for l in range(0, nc):
    idx = image == l
    r[idx] = label_colors[l, 0]
    g[idx] = label_colors[l, 1]
    b[idx] = label_colors[l, 2]
    
  rgb = np.stack([r, g, b], axis=2)
  return rgb


#recebe um arquivo ou mensagem de entrada do tipo 
#imagem1.jpeg 
#imagem2.jpeg 


imageFiles = []
for line in sys.stdin: 
    imageFiles.append(line)

imgL = Image.open(imageFiles[0][:-1])
imgR = Image.open(imageFiles[1][:-1])
imgComb = Image.new('RGB', (imgR.size[0]+imgL.size[0], imgL.size[1]))

lBox = (0,0,imgL.size[0], imgL.size[1])
lRegion = imgL.crop(lBox)
imgComb.paste(imgL, lBox)

imgRTranspose = imgR.transpose(PIL.Image.FLIP_LEFT_RIGHT)
#imgRTranspose = imgR
rBox = (imgL.size[0],0,imgL.size[0]+imgR.size[0], imgR.size[1])
rRegion = imgRTranspose.crop(rBox)
imgComb.paste(imgRTranspose, rBox)
img = imgComb

#imgComb.show()
#imgComb.save('brindeComb.jpeg')

fcn = torch.load('app/Image/model.pth').eval()
trf = T.Compose([T.Resize(256),
                 T.ToTensor(), 
                 T.Normalize(mean = [0.485, 0.456, 0.406], 
                             std = [0.229, 0.224, 0.225])])

# Pass the input through the net
inp = trf(img).unsqueeze(0)
out = fcn(inp)['out']
#print (out.shape)

om = torch.argmax(out.squeeze(), dim=0).detach().cpu().numpy()
rgb = decode_segmap(om)
#plt.imshow(img); plt.show()
#plt.imshow(rgb); plt.show()

rgba = Image.fromarray(rgb, 'RGB')
rgba = rgba.convert("RGBA")
datas = rgba.getdata()

newData = []
for item in datas:
    if item[0] == 0 and item[1] == 0 and item[2] == 0:
        newData.append((255, 255, 255, 0))
    else:
        newData.append(item)

rgba.putdata(newData)
#rgba.save("resposta.png", "PNG")
#plt.imshow(rgba); plt.show()

backGround = Image.open('app/Image/bar.jpg')
bigMask = rgba.resize((img.size[0], img.size[1]), Image.BILINEAR)
bigBG = backGround.resize((bigMask.size[0], bigMask.size[1]), Image.BILINEAR)
bigBG.paste(img, mask=bigMask.getchannel('A'))
#plt.imshow(bigBG)

bigBG.save('public/images/'+str(imgL.filename.split("input/")[1].split(".")[0])+str(imgR.filename.split("input/")[1].split(".")[0])+'.png', "PNG")
