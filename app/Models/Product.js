'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
    user(){
        return this.belongsToMany("App/Models/User").pivotTable('fridges').withPivot(["id", "num_bebidas"])
      }
    
}

module.exports = Product
