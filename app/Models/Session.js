'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Session extends Model {

    friend(){
        return this.belongsTo('App/Models/User', 'friend_id', 'id')
    }
}

module.exports = Session
