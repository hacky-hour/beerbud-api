'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with fridges
 */

const Fridge = use('App/Models/Fridge')
const Product = use('App/Models/Product')

class FridgeController {
  async addBebida ({request, response, auth}) {
    const req = request.only(["product_id", "num_bebidas"])
    const user = await auth.getUser()
    let fridge = await Fridge.findOrCreate({user_id:user.id, product_id:req.product_id}, {user_id:user.id, product_id:req.product_id})
    if(fridge.num_bebidas){
      const newNumber = fridge.num_bebidas + req.num_bebidas
      fridge.num_bebidas = Math.max(parseInt(newNumber), 0)
    } else {
      fridge.num_bebidas = Math.max(req.num_bebidas, 0)
    }
    await fridge.save()
    return fridge
  }

  async listUserProducts ({request, response, auth}) {
    try{
      const user = await auth.getUser()
      const list = await user.products().fetch()
      return list
    } catch {
      return response.status(401)
    }
  }

  async listAllProducts ({request, response}) {
    return await Product.query().fetch()
  }
  

  /**
   * Show a list of all fridges.
   * GET fridges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new fridge.
   * GET fridges/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new fridge.
   * POST fridges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single fridge.
   * GET fridges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing fridge.
   * GET fridges/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update fridge details.
   * PUT or PATCH fridges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a fridge with id.
   * DELETE fridges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = FridgeController
