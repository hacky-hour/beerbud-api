'use strict'
const Product = use("App/Models/Product")

class ProductController {
  async index({ request, response, view }) {
    const bebidas = await Product.all();
    
    return bebidas;
  }

  //C de CRUD
  async store({ request, response, view }) {
    const data = request.only(["name", "image"]);
     
    const bebida = await Product.create(data);
    
    return bebida;
  }

  //U de CRUD
  async update({ params, request, response }) {
    const bebida = await Product.findOrFail(params.id);
    const data = request.all();
    
    bebida.merge(data);
    await bebida.save();
    
    return bebida;
  }
    //D de CRUD

  async destroy({ params, request, response }) {
    const bebida = await Product.findOrFail(params.id);    
    await bebida.delete();
  }
}

module.exports = ProductController
