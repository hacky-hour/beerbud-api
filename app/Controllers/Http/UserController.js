'use strict'

const User = use("App/Models/User")
const Session = use("App/Models/Session")
const base64Img = require('base64-img');
const Python = require('../../Image/runPyScript');
const { v4: uuidv4 } = require('uuid');

class UserController {
  /**
   * Create/save a new client.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    
    const data = request.only(["username", "email", "password", "age"])
    const user = await User.create(data)
    return user

  }

  async index({request, response}) {
      return await User.query().with("friends").fetch()
  }

  async addFriend ({request, response, auth}) {
      const req = request.only(["friend_id"])
      try{
          const user = await auth.getUser()
          await user.friends().attach([req.friend_id])
          const friend = await User.find(req.friend_id)
          await friend.friends().attach([user.id])
          return user
      } catch{
          return response.status(401)
      }
  }

  async show ({request, response, auth}) {
      try{
        const user = await auth.getUser()
        const friends = await user.friends().fetch()
        return friends
      }catch {
          return response.status(401)
      }
  }

  async createSession ({request, response, auth}) {
        const req = request.only(["friend_id"])
        try{
            const user = await auth.getUser()
            const friendSessions = await Session.query().where("friend_id", user.id).andWhere("user_id", req.friend_id).fetch()
            console.log(friendSessions.toJSON().length);
            if(friendSessions.toJSON().length >= 1){
                return {...friendSessions.toJSON()[0], type: "guest"}
            } else{
                await user.sessions().attach([req.friend_id])
                const session = await Session.query().where("user_id", user.id).andWhere("friend_id", req.friend_id).fetch()
                return {...session.toJSON()[0], type: "host"}
            }
        } catch {
            return response.status(401)
        }
  }

  async uploadPhotos ({request, response, auth, params}) {
    const data = request.all()
    const entries = Object.entries(data)
    const fileName = uuidv4()
    base64Img.imgSync(entries[0][1], "app/Image/input", fileName)
    const session = await Session.find(params.id)
    entries[0][1] = fileName
    session.merge(Object.fromEntries(entries))
    await session.save()
    return session
  }

  async getResult({request, response, params}) {
    //const session = await Session.find(params.id)
    const session = await Session.query().where("id", params.id).with('friend').fetch()
    console.log(session.toJSON()[0]);
    console.log("entrou res");
    if(session.toJSON()[0].foto1 != null && session.toJSON()[0].foto2 != null){
        const pythonShell = new Python() 
        const res = await pythonShell.run(session.toJSON()[0].foto1, session.toJSON()[0].foto2)
        return {...session.toJSON()[0], status: res}
        //DELETAR SESSION
    } else {
        return {...session.toJSON()[0], status:false}
    }
  }
}

module.exports = UserController
