'use strict'

const User = use('App/Models/User')

class AuthController {

    async me ({auth}) {
        return auth.getUser()
    }

    async authenticate ({ request, auth }) { 
        const { email, password } = request.all()
        const token = await auth.attempt(email, password)
        return token
    }

    async logout({ auth, response }) {
        await auth.logout()
    }

}

module.exports = AuthController
