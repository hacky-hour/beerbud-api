'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Helpers = use('Helpers')

Route.post("/api/register", "UserController.store")
Route.post("/api/authenticate", "AuthController.authenticate")

Route.group(() => {
    Route.get("/me", "AuthController.me")
    Route.post("/addfriend", "UserController.addFriend")
    Route.get("/friends", "UserController.show")
    Route.post("/createsessions", "UserController.createSession")
    Route.post("/uploadPhoto/:id", "UserController.uploadPhotos")
    Route.get("/result/:id", "UserController.getResult")
    //CRUD do Product
    //Route.resource('bebidas', 'ProductController').apiOnly();
    //controle do freezer
    Route.post("/newBebidaType", "ProductController.store")
    Route.post("/addBebida", "FridgeController.addBebida")
    Route.get("/listAllProducts", "FridgeController.listAllProducts")
    Route.get("/listUserProducts", "FridgeController.listUserProducts")
    Route.get("/users", "UserController.index")

    
}).prefix("/api")

Route.get("/teste", () => {
    return Helpers.tmpPath('b7d0b0c4-6395-4b4e-9f33-95fa2cb764d56296f64e-1e13-49b0-b26d-df65b1e7bbb4.png')
})
