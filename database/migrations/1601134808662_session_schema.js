'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessionSchema extends Schema {
  up () {
    this.create('sessions', (table) => {
      table.increments()
      table.integer("user_id").unsigned().references("id").inTable("users").onDelete('CASCADE')
      table.integer("friend_id").unsigned().references("id").inTable("users").onDelete('CASCADE')
      table.string("foto1")
      table.string("foto2")
      table.timestamps()
    })
  }

  down () {
    this.drop('sessions')
  }
}

module.exports = SessionSchema
