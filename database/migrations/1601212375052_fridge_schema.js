'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FridgeSchema extends Schema {
  up () {
    this.create('fridges', (table) => {
      table.increments()
      
      table.integer('user_id').unsigned().references("id").inTable("users").onDelete('CASCADE')
      table.integer('product_id').unsigned().references("id").inTable("products").onDelete('CASCADE')
      table.integer('num_bebidas')
      table.timestamps()
    })
  }

  down () {
    this.drop('fridges')
  }
}

module.exports = FridgeSchema
