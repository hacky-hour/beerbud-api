'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserUserSchema extends Schema {
  up () {
    this.create('user_user', (table) => {
      table.increments()
      table.integer("user_id").unsigned().references("id").inTable("users").onDelete('CASCADE')
      table.integer("friend_id").unsigned().references("id").inTable("users").onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('user_users')
  }
}

module.exports = UserUserSchema
