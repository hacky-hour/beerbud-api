'use strict'

/*
|--------------------------------------------------------------------------
| ProductSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Product = use("App/Models/Product")
const product = [
    {name: "Antarctica", image: "logos/logo_antarctica.jpg"},
    {name: "Bohemia", image: "logos/logo_bohemia_novo.jpg"},
    {name: "Brahma", image: "logos/logo_brahma.jpg"},
    {name: "Budweiser", image: "logos/logo_bud.jpg"},
    {name: "Caracu", image: "logos/logo_caracu.jpg"},
    {name: "Corona", image: "logos/logo_corona.jpg"},
    {name: "Original", image: "logos/logo_original.jpg"},
    {name: "Serrana", image: "logos/logo_serrana.jpg"},
    {name: "Skol", image: "logos/logo_skol.jpg"},
    {name: "Stella Artois", image: "logos/logo_stella_artois.jpg"},
]

class ProductSeeder {
  async run () {
    await Product.createMany(product)
  }
}

module.exports = ProductSeeder

